﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UsuarioModulo.Models
{
    public class Usuario
    {
        public int id { get; set; }
        public string usuario { get; set; }
        public string desactivado { get; set; }
        public DateTime? fecha_creacion { get; set; } = DateTime.Now;
        public DateTime? fecha_actualizacion { get; set; }
    }
}
