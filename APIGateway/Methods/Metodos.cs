﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace APIGateway.Methods
{
    public static class Metodos
    {
         static void AddResourceOcelot(this IConfigurationBuilder config, string path = "Resource")
        {
            List<object> route = new List<object>();
            object globalConfiguration = null;
            object aggregates = null;
            object swaggerEndPoints = null;
            foreach (var jsonFilename in Directory.EnumerateFiles(path, "*.json", SearchOption.AllDirectories))
            {
                dynamic jsonObj = JsonConvert.DeserializeObject(File.ReadAllText(jsonFilename));
                try
                {
                    if (jsonObj["Routes"] != null)
                        route.Add(string.Join(',', jsonObj["Routes"]));
                    if (jsonObj["GlobalConfiguration"] != null)
                        globalConfiguration = jsonObj["GlobalConfiguration"];
                    if (jsonObj["Aggregates"] != null)
                        aggregates = jsonObj["Aggregates"];
                    if (jsonObj["SwaggerEndPoints"] != null)
                        swaggerEndPoints = jsonObj["SwaggerEndPoints"];
                }
                catch (Exception ex)
                {
                }
            }
            var oData = String.Join(',', route);
            string oGlobalConfiguration = string.Empty;
            string oAggregates = string.Empty;
            string oConfiguration = string.Empty;
            string oSwaggerEndPoints = string.Empty;

            string oRoutes = $" \"Routes\": [" +
                      $"     { oData.ToString() } " +
                      $"   ]";

            if (globalConfiguration != null)
                oGlobalConfiguration = $"  \"GlobalConfiguration\": " +
                                       $"     { globalConfiguration.ToString() } ";

            if (aggregates != null)
                oAggregates = $"  \"Aggregates\": " +
                             $"     { aggregates.ToString() }";

            if (swaggerEndPoints != null)
                oSwaggerEndPoints = $"  \"SwaggerEndPoints\": " +
                                    $"     { swaggerEndPoints.ToString() }";

            oConfiguration += $"{{" +
                              $"   { oRoutes }, " +
                              $"   { oSwaggerEndPoints }, " +
                              $"   { oAggregates }, " +
                              $"   { oGlobalConfiguration }" +
                              $"}}";

            config.AddJsonStream(new MemoryStream(Encoding.UTF8.GetBytes(oConfiguration)));
        }
    }
}
